#include <cstdlib>

#include "List.h"
using namespace std;

int main(int argc, char** argv){
    List myList;
    myList.AddNode(4);
    myList.AddNode(3);
    myList.AddNode(5);

    myList.PrintList();

    myList.DeleteNode(4);

    myList.PrintList();
}