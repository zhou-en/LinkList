/*
File List.h

*/
#ifndef LIST_H
#define LIST_H
class List{
    private:    // all parts can only be access by public function
        // struct node{
        //     int data;
        //     node* next; // point another node in the list
        // };
        // // allow assign node using nodePrt
        // typedef struct node* nodePtr;

        // above code equilent to following
        typedef struct node{
            int data;
            node* next; // point another node in the list
        }* nodePtr;
        
        //create few nodes
        nodePtr head;
        nodePtr curr;
        nodePtr temp;
    public: // This is where the functions go, which are used to access private data
        List(); //constructor
        void AddNode(int addData);
        void DeleteNode(int delData);
        void PrintList();
};

#endif /*LIST_H*/